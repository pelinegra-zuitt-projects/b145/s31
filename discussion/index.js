//UNINSTALL express
//npm uninstall express

//npm i express@4.17.2

//Creating a simple server in Express.js
//Creating a server using Node.js

// Let http = require('http');
// http.createServer((req, res) =>{
// 	if(request.url =='/' && req.method == 'GET'){
// 		res.writeHead(200, {'Content-Type' : 'text/plain'})
// 		res.end('Data receeived')
// 	}

// });


//Creatng a simple server in Express.js

const express = require('express');

const app = express();

const port = 4000;

// //Middlewares ==> servers which provide common services and capabilities to our server
app.use(express.json())
app.use(express.urlencoded({extended: true}));

// app.listen(port, () => console.log(`Server running at port ${port}`))


let users = [
	{
		email: "attackTitan@mail.com",
		username: "thisIsEren",
		password: "notABird",
		isAdmin: false
	},
	{
		email: "mrClean@mail.com",
		username: "AckermanLevi",
		password: "stillAlive",
		isAdmin: true
	},
	{
		email: "redScarf@mail.com",
		username: "Mikasa12",
		password: "whereIsEren",
		isAdmin: false
	},
];

let loggedUser;

app.get('/',(req,res) => {res.send('Hello World')});

app.get('/hello',(req,res) => {res.send('Hello from 145')});


app.post('/',(req, res) => {

	console.log(req.body)
	res.send(`Hello I am ${req.body.name}, my age is ${req.body.age}. I could be described as ${req.body.description}`)
});


//REGISTER ROUTE
app.post('/users',(req, res)=> {
	console.log(req.body);
	let newUser = {
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

users.push(newUser);
console.log(users);
res.send(`User ${req.body.username} has successfully registered`)
});

//LOG IN 

app.post('/users/login', (req, res) => {

	console.log(req.body);

	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password
	})

	if(foundUser !== undefined){
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});
		
		foundUser.index = foundUserIndex

		loggedUser = foundUser

		res.send(`Thank you for logging in.`)

	} else {

		loggedUser = foundUser
		res.send('Sorry, wrong credentials')
	}
});




//CHANGE PASSWORD

app.put('/users/change-password', (req, res) => {

	let message; 

	for(let i=0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password
			
			message = `User ${req.body.username}'s password has been changed`
			
			break;

		} else {
			message = `User does not exist`
		}

	}
	res.send(message)
})




//mini activity

//1. Make a route /hello as an endpoint
//2. send a message saying hello from batch 145

//Change message from POST method ==> res.send = Hello I am <namefromRequest>, my age is <ageFromRequest>. I could be described as <descriptionFromrequest>
//Send your postman output in Hangouts



app.listen(port,()=> console.log(`Server running at port ${port}`));
