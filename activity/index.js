const express = require('express');

const app = express();

const port = 4000;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//PRINT OUT SIMPLE MESSAGE --> Get
app.get('/home',(req,res) => {res.send('Hello from me')});


//GET ROUTE that will access the /users route ---> Retrieve all the users in the mock database
let users = [
	{
		email: "Iam@mail.com",
		username: "Iam",
		password: "sUddEnLy",
	
	},
	{
		email: "none@mail.com",
		username: "noneTheLess",
		password: "tRendz",
		
	},
	{
		email: "cornerman@mail.com",
		username: "jogging3",
		password: "Entren",
		
	},
];

app.get('/users',(req,res) => {res.send(users)});


//DELETE ROUTE--> to remove user from the database

app.delete('/users/delete', (req, res) => {

	console.log(req.body);

	let deleteUser = {

		username: req.body.username,

	};


	res.send(`User ${req.body.username} is successfully removed`)

});

























//listenport
app.listen(port, () => console.log(`Server running at port ${port}`));